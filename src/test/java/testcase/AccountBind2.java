package testcase;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import base.BaseTool;
import business.UserAccount;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：绑定或变更手机号与绑定第三方账号、解绑第三方账号
 * 参数：thirdType绑定类型(1微博、2微信、3qq、4手机号)
 * */
public class AccountBind2 {
	@BeforeMethod
	public void beforeMethod() {
		UserAccount.createUser(2);
	}

	@AfterMethod
	public void afterMethod() {
		UserAccount.delUser();
	}

	// 用例描述：用户绑定微博、微信、QQ
	@Test()
	public void AccountBind2_case1() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "1");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		//
		JSONObject data2 = new JSONObject();
		data2.put("userId", UserAccount.userId.get(0));
		data2.put("thirdType", "2");
		data2.put("openid", openID1);
		data2.put("unionidWx", "");
		data2.put("msCode", "");
		JSONObject respson2 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data2);
		assertEquals(respson2.getString("code"), "1000");
		assertEquals(respson2.getString("msg"), "成功");
		//
		JSONObject data3 = new JSONObject();
		data3.put("userId", UserAccount.userId.get(0));
		data3.put("thirdType", "3");
		data3.put("openid", openID1);
		data3.put("unionidWx", "");
		data3.put("msCode", "");
		JSONObject respson3 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data3);
		assertEquals(respson3.getString("code"), "1000");
		assertEquals(respson3.getString("msg"), "成功");
	}

	// 用例描述：用户1绑定微博1
	@Test()
	public void AccountBind2_case11() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "1");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：用户1绑定不存在微博
	@Test()
	public void AccountBind2_case12() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "1");
		data1.put("openid", "");
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "参数验证错误");
	}

	// 用例描述：用户1绑定微博1，用户1再绑定微博1
	@Test()
	public void AccountBind2_case13() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "1");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		//
		JSONObject data2 = new JSONObject();
		data2.put("userId", UserAccount.userId.get(0));
		data2.put("thirdType", "1");
		data2.put("openid", openID1);
		data2.put("unionidWx", "");
		data2.put("msCode", "");
		JSONObject respson2 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data2);
		assertEquals(respson2.getString("code"), "4060");
		assertEquals(respson2.getString("msg"), "该信息已被其他用户使用");
	}

	// 用例描述：用户1绑定微博1，用户2再绑定微博1
	@Test()
	public void AccountBind2_case14() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "1");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		//
		JSONObject data2 = new JSONObject();
		data2.put("userId", UserAccount.userId.get(1));
		data2.put("thirdType", "1");
		data2.put("openid", openID1);
		data2.put("unionidWx", "");
		data2.put("msCode", "");
		JSONObject respson2 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data2);
		assertEquals(respson2.getString("code"), "4060");
		assertEquals(respson2.getString("msg"), "该信息已被其他用户使用");
	}

	// 用例描述：用户1绑定微博1，用户1再解绑微博1，用户1再绑定微博1
	@Test()
	public void AccountBind2_case15() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "1");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		//
		JSONObject data2 = new JSONObject();
		data2.put("userId", UserAccount.userId.get(0));
		data2.put("thirdType", "1");
		JSONObject respson2 = BaseHttpClient.sendGetRequest(BaseData.URL_解绑第三方账号, data2);
		assertEquals(respson2.getString("code"), "1000");
		assertEquals(respson2.getString("msg"), "成功");
		//
		JSONObject data3 = new JSONObject();
		data3.put("userId", UserAccount.userId.get(0));
		data3.put("thirdType", "1");
		data3.put("openid", openID1);
		data3.put("unionidWx", "");
		data3.put("msCode", "");
		JSONObject respson3 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data3);
		assertEquals(respson3.getString("code"), "1000");
		assertEquals(respson3.getString("msg"), "成功");
	}

	// 用例描述：用户1绑定微博1，用户1再解绑微博1，用户2再绑定微博1
	@Test()
	public void AccountBind2_case16() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "1");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		//
		JSONObject data2 = new JSONObject();
		data2.put("userId", UserAccount.userId.get(0));
		data2.put("thirdType", "1");
		JSONObject respson2 = BaseHttpClient.sendGetRequest(BaseData.URL_解绑第三方账号, data2);
		assertEquals(respson2.getString("code"), "1000");
		assertEquals(respson2.getString("msg"), "成功");
		//
		JSONObject data3 = new JSONObject();
		data3.put("userId", UserAccount.userId.get(1));
		data3.put("thirdType", "1");
		data3.put("openid", openID1);
		data3.put("unionidWx", "");
		data3.put("msCode", "");
		JSONObject respson3 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data3);
		assertEquals(respson3.getString("code"), "1000");
		assertEquals(respson3.getString("msg"), "成功");
	}

	// 用例描述：用户1绑定微信1
	@Test()
	public void AccountBind2_case21() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "2");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：用户1绑定不存在微信
	@Test()
	public void AccountBind2_case22() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "2");
		data1.put("openid", "");
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "参数验证错误");
	}

	// 用例描述：用户1绑定微信1，用户1再绑定微信1
	@Test()
	public void AccountBind2_case23() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "2");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		//
		JSONObject data2 = new JSONObject();
		data2.put("userId", UserAccount.userId.get(0));
		data2.put("thirdType", "2");
		data2.put("openid", openID1);
		data2.put("unionidWx", "");
		data2.put("msCode", "");
		JSONObject respson2 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data2);
		assertEquals(respson2.getString("code"), "4060");
		assertEquals(respson2.getString("msg"), "该信息已被其他用户使用");
	}

	// 用例描述：用户1绑定微信1，用户2再绑定微信1
	@Test()
	public void AccountBind2_case24() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "2");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		//
		JSONObject data2 = new JSONObject();
		data2.put("userId", UserAccount.userId.get(1));
		data2.put("thirdType", "2");
		data2.put("openid", openID1);
		data2.put("unionidWx", "");
		data2.put("msCode", "");
		JSONObject respson2 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data2);
		assertEquals(respson2.getString("code"), "4060");
		assertEquals(respson2.getString("msg"), "该信息已被其他用户使用");
	}

	// 用例描述：用户1绑定微信1，用户1再解绑微信1，用户1再绑定微信1
	@Test()
	public void AccountBind2_case25() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "2");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		//
		JSONObject data2 = new JSONObject();
		data2.put("userId", UserAccount.userId.get(0));
		data2.put("thirdType", "2");
		JSONObject respson2 = BaseHttpClient.sendGetRequest(BaseData.URL_解绑第三方账号, data2);
		assertEquals(respson2.getString("code"), "1000");
		assertEquals(respson2.getString("msg"), "成功");
		//
		JSONObject data3 = new JSONObject();
		data3.put("userId", UserAccount.userId.get(0));
		data3.put("thirdType", "2");
		data3.put("openid", openID1);
		data3.put("unionidWx", "");
		data3.put("msCode", "");
		JSONObject respson3 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data3);
		assertEquals(respson3.getString("code"), "1000");
		assertEquals(respson3.getString("msg"), "成功");
	}

	// 用例描述：用户1绑定微信1，用户1再解绑微信1，用户2再绑定微信1
	@Test()
	public void AccountBind2_case26() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "2");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		//
		JSONObject data2 = new JSONObject();
		data2.put("userId", UserAccount.userId.get(0));
		data2.put("thirdType", "2");
		JSONObject respson2 = BaseHttpClient.sendGetRequest(BaseData.URL_解绑第三方账号, data2);
		assertEquals(respson2.getString("code"), "1000");
		assertEquals(respson2.getString("msg"), "成功");
		//
		JSONObject data3 = new JSONObject();
		data3.put("userId", UserAccount.userId.get(1));
		data3.put("thirdType", "2");
		data3.put("openid", openID1);
		data3.put("unionidWx", "");
		data3.put("msCode", "");
		JSONObject respson3 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data3);
		assertEquals(respson3.getString("code"), "1000");
		assertEquals(respson3.getString("msg"), "成功");
	}

	// 用例描述：用户1绑定QQ1
	@Test()
	public void AccountBind2_case31() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "3");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：用户1绑定不存在QQ1
	@Test()
	public void AccountBind2_case32() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "3");
		data1.put("openid", "");
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "参数验证错误");
	}

	// 用例描述：用户1绑定QQ1，用户1再绑定QQ1
	@Test()
	public void AccountBind2_case33() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "3");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		//
		JSONObject data2 = new JSONObject();
		data2.put("userId", UserAccount.userId.get(0));
		data2.put("thirdType", "3");
		data2.put("openid", openID1);
		data2.put("unionidWx", "");
		data2.put("msCode", "");
		JSONObject respson2 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data2);
		assertEquals(respson2.getString("code"), "4060");
		assertEquals(respson2.getString("msg"), "该信息已被其他用户使用");
	}

	// 用例描述：用户1绑定QQ1，用户2再绑定QQ1
	@Test()
	public void AccountBind2_case34() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "3");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		//
		JSONObject data2 = new JSONObject();
		data2.put("userId", UserAccount.userId.get(1));
		data2.put("thirdType", "3");
		data2.put("openid", openID1);
		data2.put("unionidWx", "");
		data2.put("msCode", "");
		JSONObject respson2 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data2);
		assertEquals(respson2.getString("code"), "4060");
		assertEquals(respson2.getString("msg"), "该信息已被其他用户使用");
	}

	// 用例描述：用户1绑定QQ1，用户1再解绑QQ1，用户1再绑定QQ1
	@Test()
	public void AccountBind2_case35() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "3");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		//
		JSONObject data2 = new JSONObject();
		data2.put("userId", UserAccount.userId.get(0));
		data2.put("thirdType", "3");
		JSONObject respson2 = BaseHttpClient.sendGetRequest(BaseData.URL_解绑第三方账号, data2);
		assertEquals(respson2.getString("code"), "1000");
		assertEquals(respson2.getString("msg"), "成功");
		//
		JSONObject data3 = new JSONObject();
		data3.put("userId", UserAccount.userId.get(0));
		data3.put("thirdType", "3");
		data3.put("openid", openID1);
		data3.put("unionidWx", "");
		data3.put("msCode", "");
		JSONObject respson3 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data3);
		assertEquals(respson3.getString("code"), "1000");
		assertEquals(respson3.getString("msg"), "成功");
	}

	// 用例描述：用户1绑定QQ1，用户1再解绑QQ1，用户2再绑定QQ1
	@Test()
	public void AccountBind2_case36() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "3");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		//
		JSONObject data2 = new JSONObject();
		data2.put("userId", UserAccount.userId.get(0));
		data2.put("thirdType", "3");
		JSONObject respson2 = BaseHttpClient.sendGetRequest(BaseData.URL_解绑第三方账号, data2);
		assertEquals(respson2.getString("code"), "1000");
		assertEquals(respson2.getString("msg"), "成功");
		//
		JSONObject data3 = new JSONObject();
		data3.put("userId", UserAccount.userId.get(1));
		data3.put("thirdType", "3");
		data3.put("openid", openID1);
		data3.put("unionidWx", "");
		data3.put("msCode", "");
		JSONObject respson3 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data3);
		assertEquals(respson3.getString("code"), "1000");
		assertEquals(respson3.getString("msg"), "成功");
	}

	// 用例描述：用户1变更手机1
	@Test()
	public void AccountBind2_case41() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "4");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "1111");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：用户1变更手机1，无验证码
	@Test()
	public void AccountBind2_case42() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "4");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "参数验证错误");
	}

	// 用例描述：用户1变更不存在手机1
	@Test()
	public void AccountBind2_case43() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "4");
		data1.put("openid", "");
		data1.put("unionidWx", "");
		data1.put("msCode", "1111");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "参数验证错误");
	}

	// 用例描述：用户1变更手机1，用户1再变更手机1
	@Test()
	public void AccountBind2_case44() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "4");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "1111");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		//
		JSONObject data2 = new JSONObject();
		data2.put("userId", UserAccount.userId.get(0));
		data2.put("thirdType", "4");
		data2.put("openid", openID1);
		data2.put("unionidWx", "");
		data2.put("msCode", "1111");
		JSONObject respson2 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data2);
		assertEquals(respson2.getString("code"), "4060");
		assertEquals(respson2.getString("msg"), "该信息已被其他用户使用");
	}

	// 用例描述：用户1变更手机1，用户2再变更手机1
	@Test()
	public void AccountBind2_case45() {
		String openID1 = BaseTool.getStringByChar(32);
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("thirdType", "4");
		data1.put("openid", openID1);
		data1.put("unionidWx", "");
		data1.put("msCode", "1111");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		//
		JSONObject data2 = new JSONObject();
		data2.put("userId", UserAccount.userId.get(1));
		data2.put("thirdType", "4");
		data2.put("openid", openID1);
		data2.put("unionidWx", "");
		data2.put("msCode", "1111");
		JSONObject respson2 = BaseHttpClient.sendGetRequest(BaseData.URL_绑定或变更手机号与绑定第三方账号, data2);
		assertEquals(respson2.getString("code"), "4060");
		assertEquals(respson2.getString("msg"), "该信息已被其他用户使用");
	}
}