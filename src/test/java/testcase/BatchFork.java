package testcase;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import business.UserAccount;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：批量关注
 * 参数：
 * 备注：已与朱兴军、葛玉琦确认，重复关注等情况不考虑
 * */
public class BatchFork {
	@BeforeMethod
	public void beforeMethod() {
		UserAccount.createUser(3);
	}

	@AfterMethod
	public void afterMethod() {
		UserAccount.delUser();
	}

	// 用例描述：批量关注多人
	@Test()
	public void BatchFork_case1() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("mainUserIds", UserAccount.userId.get(1) + "," + UserAccount.userId.get(2));
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_批量关注, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：批量关注空
	@Test()
	public void BatchFork_case2() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("mainUserIds", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_批量关注, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "参数验证错误");
	}
}