package testcase;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import business.UserAccount;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：获取行政区列表
 * 参数：
 * */
public class GetDistrictList {
	@BeforeMethod
	public void beforeMethod() {
		UserAccount.createUser(1);
	}

	@AfterMethod
	public void afterMethod() {
		UserAccount.delUser();
	}

	// 用例描述：正常
	@Test()
	public void GetDistrictList_case1() {
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_获取行政区列表, new JSONObject());
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		assertEquals(respson1.getJSONArray("data").size() + "", "3");
		assertEquals(respson1.getJSONArray("data").getJSONObject(0).getString("name"), "香港");
		assertEquals(respson1.getJSONArray("data").getJSONObject(1).getString("name"), "澳门");
		assertEquals(respson1.getJSONArray("data").getJSONObject(2).getString("name"), "台湾");
	}
}