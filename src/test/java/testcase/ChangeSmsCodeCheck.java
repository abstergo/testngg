package testcase;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import business.UserAccount;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：已绑定手机号进行变更手机号旧手机号短信验证
 * 参数：
 * */
public class ChangeSmsCodeCheck {
	@BeforeMethod
	public void beforeMethod() {
		UserAccount.createUser(1);
	}

	@AfterMethod
	public void afterMethod() {
		UserAccount.delUser();
	}

	// 用例描述：正常验证码
	@Test()
	public void ChangeSmsCodeCheck_case1() {
		JSONObject data1 = new JSONObject();
		data1.put("mobile", UserAccount.mobile.get(0));
		data1.put("msCode", "123");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_已绑定手机号进行变更手机号旧手机号短信验证, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：空验证码
	@Test()
	public void ChangeSmsCodeCheck_case2() {
		JSONObject data1 = new JSONObject();
		data1.put("mobile", UserAccount.mobile.get(0));
		data1.put("msCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_已绑定手机号进行变更手机号旧手机号短信验证, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "参数验证错误");
	}
}