package testcase;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import base.BaseTool;
import business.UserAccount;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：手机号登陆(登录或注册、等于11位时为手机号登录、不等11位时为内部帐号+密码登录)
 * 参数：
 * */
public class ZZZAssert_UserLogin {
	// 用例描述：11位已存在手机号登录
	@Test()
	public void UserLogin_case1() {
		UserAccount.createUser(1);
		//
		JSONObject data1 = new JSONObject();
		data1.put("deviceNo", "123");
		data1.put("deviceToken", "123");
		data1.put("mobile", UserAccount.mobile.get(0));
		data1.put("msCode", "1111");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_手机号登陆, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：11位未存在手机号注册
	@Test()
	public void UserLogin_case2() {
		JSONObject data1 = new JSONObject();
		data1.put("deviceNo", "123");
		data1.put("deviceToken", "123");
		data1.put("mobile", BaseTool.getStringByTime(11));
		data1.put("msCode", "1111");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_手机号登陆, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：10位手机号登录
	@Test()
	public void UserLogin_case3() {
		JSONObject data1 = new JSONObject();
		data1.put("deviceNo", "123");
		data1.put("deviceToken", "123");
		data1.put("mobile", BaseTool.getStringByTime(10));
		data1.put("msCode", "1111");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_手机号登陆, data1);
		assertEquals(respson1.getString("code"), "4050");
		assertEquals(respson1.getString("msg"), "用户信息不存在");
	}

	// 用例描述：12位手机号登录
	@Test()
	public void UserLogin_case4() {
		JSONObject data1 = new JSONObject();
		data1.put("deviceNo", "123");
		data1.put("deviceToken", "123");
		data1.put("mobile", BaseTool.getStringByTime(12));
		data1.put("msCode", "1111");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_手机号登陆, data1);
		assertEquals(respson1.getString("code"), "4050");
		assertEquals(respson1.getString("msg"), "用户信息不存在");
	}

	// 用例描述：正确内部帐号+正确密码登录
	@Test()
	public void UserLogin_case5() {
		JSONObject data1 = new JSONObject();
		data1.put("deviceNo", "123");
		data1.put("deviceToken", "123");
		data1.put("mobile", "911");
		data1.put("msCode", "Admin1020");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_手机号登陆, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：正确内部帐号+错误密码登录
	@Test()
	public void UserLogin_case6() {
		JSONObject data1 = new JSONObject();
		data1.put("deviceNo", "123");
		data1.put("deviceToken", "123");
		data1.put("mobile", "911");
		data1.put("msCode", "Admin");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_手机号登陆, data1);
		assertEquals(respson1.getString("code"), "4051");
		assertEquals(respson1.getString("msg"), "用户密码错误");
	}

	// 用例描述：错误内部帐号+正确密码登录
	@Test()
	public void UserLogin_case7() {
		JSONObject data1 = new JSONObject();
		data1.put("deviceNo", "123");
		data1.put("deviceToken", "123");
		data1.put("mobile", "900");
		data1.put("msCode", "Admin1020");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_手机号登陆, data1);
		assertEquals(respson1.getString("code"), "4050");
		assertEquals(respson1.getString("msg"), "用户信息不存在");
	}
}