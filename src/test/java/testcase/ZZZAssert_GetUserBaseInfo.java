package testcase;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import business.UserAccount;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：获取用户基础信息
 * 参数：
 * */
public class ZZZAssert_GetUserBaseInfo {
	@BeforeMethod
	public void beforeMethod() {
		UserAccount.createUser(1);
	}

	@AfterMethod
	public void afterMethod() {
		UserAccount.delUser();
	}

	// 用例描述：获取存在用户基础信息
	@Test()
	public void GetUserBaseInfo_case1() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_获取用户基础信息, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：获取不存在用户基础信息
	@Test()
	public void GetUserBaseInfo_case2() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", "000000");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_获取用户基础信息, data1);
		assertEquals(respson1.getString("code"), "2001");
		assertEquals(respson1.getString("msg"), "未知异常");
	}

	// 用例描述：获取已删除用户基础信息
	@Test()
	public void GetUserBaseInfo_case3() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		UserAccount.delUserByUerID(UserAccount.userId.get(0));
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_获取用户基础信息, data1);
		assertEquals(respson1.getString("code"), "2001");
		assertEquals(respson1.getString("msg"), "未知异常");
	}
}