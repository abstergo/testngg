package base;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import net.sf.json.JSONObject;

@SuppressWarnings("all")
public class BaseHttpClient {
	public static String responseString;
	public static JSONObject responseJSONObject;
	public static String token = "", consume = "";

	/** 手机号登录，返回token与consume */
	public static void loginPhone(String user) {
		JSONObject data = new JSONObject();
		data.put("deviceToken", "AkM2v3KPD5Fcu60oHKcET-81-sI4Jf91bc6Ul5eGn-mU");
		data.put("mobile", user);
		data.put("msCode", "1111");
		data.put("deviceNo", "fdea0378d159ce8d516aaa7f383758e717a8423c");

		JSONObject json = BaseHttpClient.sendGetRequest(BaseData.URL_手机号登录, data);
		token = json.getJSONObject("data").getString("token");
		consume = json.getJSONObject("data").getString("userId");
		// System.out.println("登录成功：" + token + "、" + consume);
	}

	/** 新浪登录，返回token与consume */
	public static void loginSina() {
		JSONObject data = new JSONObject();
		data.put("nickName", "你的惊喜-礼物3062");
		data.put("thirdType", 1);
		data.put("unionidWx", "");
		data.put("deviceToken", "AkM2v3KPD5Fcu60oHKcET-81-sI4Jf91bc6Ul5eGn-mU");
		data.put("headImg", "http://tva2.sinaimg.cn/crop.0.0.180.180.50/4b6b83a9jw1e8qgp5bmzyj2050050aa8.jpg");
		data.put("openid", "1265337257");
		data.put("deviceNo", "fdea0378d159ce8d516aaa7f383758e717a8423c");

		JSONObject json = BaseHttpClient.sendGetRequest(BaseData.URL_第三方登录, data);
		token = json.getJSONObject("data").getString("token");
		consume = json.getJSONObject("data").getString("userId");
		// System.out.println("登录成功：" + token + "、" + consume);
	}

	/** 微信登录，返回token与consume */
	public static void loginWeiXin() {
		JSONObject data = new JSONObject();
		data.put("nickName", "王中南");
		data.put("thirdType", 2);
		data.put("unionidWx", "outC8wI3jigfiW_hwqx3axkHX6vM");
		data.put("deviceToken", "AkM2v3KPD5Fcu60oHKcET-81-sI4Jf91bc6Ul5eGn-mU");
		data.put("headImg", "http://wx.qlogo.cn/mmopen/Q3auHgzwzM4BxVMiaiauzVmDD9rFczrMpozqxj28nOUYCl9rxRRXUZicRcJWcZv8ibnbF3pHST12ubiczWqNY57hibddhThb8Mm3tbNMDbuphtibpw/0");
		data.put("openid", "ovM5txN7Z94heH-hXqb9yOySpsK8");
		data.put("deviceNo", "fdea0378d159ce8d516aaa7f383758e717a8423c");

		JSONObject json = BaseHttpClient.sendGetRequest(BaseData.URL_第三方登录, data);
		token = json.getJSONObject("data").getString("token");
		consume = json.getJSONObject("data").getString("userId");
		// System.out.println("登录成功：" + token + "、" + consume);
	}

	/** QQ登录，返回token与consume */
	public static void loginQQ() {
		JSONObject data = new JSONObject();
		data.put("nickName", "蜗牛骑火箭");
		data.put("thirdType", 3);
		data.put("unionidWx", "");
		data.put("deviceToken", "AkM2v3KPD5Fcu60oHKcET-81-sI4Jf91bc6Ul5eGn-mU");
		data.put("headImg", "http://q.qlogo.cn/qqapp/1105811896/A6F941449766ED2FA741F5E53F54AF39/100");
		data.put("openid", "A6F941449766ED2FA741F5E53F54AF39");
		data.put("deviceNo", "fdea0378d159ce8d516aaa7f383758e717a8423c");

		JSONObject json = BaseHttpClient.sendGetRequest(BaseData.URL_第三方登录, data);
		token = json.getJSONObject("data").getString("token");
		consume = json.getJSONObject("data").getString("userId");
		// System.out.println("登录成功：" + token + "、" + consume);
	}

	/** 自动登录，返回token与consume */
	public static void loginAuto(String user) {
		JSONObject data = new JSONObject();
		data.put("deviceToken", "AkM2v3KPD5Fcu60oHKcET-81-sI4Jf91bc6Ul5eGn-mU");
		data.put("mobile", user);
		data.put("msCode", "1111");
		data.put("deviceNo", "fdea0378d159ce8d516aaa7f383758e717a8423c");

		JSONObject json = BaseHttpClient.sendGetRequest(BaseData.URL_手机号登录, data);
		token = json.getJSONObject("data").getString("token");
		consume = json.getJSONObject("data").getString("userId");
		System.out.println(token + "、" + consume);

		JSONObject data2 = new JSONObject();
		data2.put("token", token);
		data2.put("deviceToken", "AkM2v3KPD5Fcu60oHKcET-81-sI4Jf91bc6Ul5eGn-mU");
		JSONObject json2 = BaseHttpClient.sendGetRequest(BaseData.URL_自动登录, data2);
		token = json2.getJSONObject("data").getString("token");
		consume = json2.getJSONObject("data").getString("userId");
		// System.out.println("登录成功：" + token + "、" + consume);
	}

	/** 发送get请求，返回响应JSONObject */
	public static JSONObject sendGetRequest(String url, JSONObject data) {
		String msg = BaseTool.getMD5(consume + "_key_" + data.toString());
		String newData = BaseTool.getURLEncoder(data.toString());
		String newURL = url + "?consume=" + consume + "&msg=" + msg + "&data=" + newData + "&time=" + System.currentTimeMillis();

		try {
			HttpClient client = HttpClients.createDefault();

			HttpGet get = new HttpGet(newURL);
			get.setHeader("authinfo", token);
			get.setHeader("buriedPoint", "{\"channelNo\":\"HF17\",\"deviceType\":\"android\",\"deviceModel\":\"HUAWEI NXT-AL10\",\"uid\":\"\",\"deviceToken\":\"53a3b463dad547609241fa5d76f5e237\",\"appVer\":\"1.1.20.301\",\"devImei\":\"869906020513618\",\"sysVersion\":\"6.0\",\"netType\":\"5\",\"clp\":\"192.168.3.114\",\"loc\":\"\u5317\u4eac\u5e02\",\"appCode\":\"27\"}");

			HttpResponse response = client.execute(get);

			String responseStatusCode = response.getStatusLine().getStatusCode() + "";

			if (responseStatusCode.equals("200")) {
				HttpEntity responseEntity = response.getEntity();

				responseString = EntityUtils.toString(responseEntity);
				responseJSONObject = JSONObject.fromObject(responseString);
				// System.out.println(responseString);
			} else {
				responseJSONObject.put("errorStatusCode", responseStatusCode);
			}
			System.out.println(responseString);

			String executeTime = BaseTool.getCurrentDate("yyyy-MM-dd HH:mm:ss");
			String testcase = Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName();
			BaseTool.logInfo(executeTime, testcase, url, responseStatusCode, BaseTool.jsonFormate(data.toString()), BaseTool.jsonFormate(responseString));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseJSONObject;
	}
}