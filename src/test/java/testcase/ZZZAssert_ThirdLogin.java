package testcase;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：第三方登陆
 * 参数：deviceNo设备号、thirdType第三方类型(1微博、2微信、3qq)、openid第三方的openid
 * 参数：deviceToken推送token、unionidWx微信的唯一标识unionid、nickName昵称、headImg头像
 * */
public class ZZZAssert_ThirdLogin {
	// 用例描述：微博登录
	@Test()
	public void ThirdLogin_case1() {
		JSONObject data1 = new JSONObject();
		data1.put("deviceNo", "123");
		data1.put("thirdType", "1");
		data1.put("openid", "123");
		data1.put("deviceToken", "123");
		data1.put("unionidWx", "");
		data1.put("nickName", "王中南");
		data1.put("headImg", "http://www.baidu.com");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_第三方登陆, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：微信登录
	@Test()
	public void ThirdLogin_case2() {
		JSONObject data1 = new JSONObject();
		data1.put("deviceNo", "123");
		data1.put("thirdType", "2");
		data1.put("openid", "123");
		data1.put("deviceToken", "123");
		data1.put("unionidWx", "123");
		data1.put("nickName", "王中南");
		data1.put("headImg", "http://www.baidu.com");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_第三方登陆, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：qq登录
	@Test()
	public void ThirdLogin_case3() {
		JSONObject data1 = new JSONObject();
		data1.put("deviceNo", "123");
		data1.put("thirdType", "3");
		data1.put("openid", "123");
		data1.put("deviceToken", "123");
		data1.put("unionidWx", "");
		data1.put("nickName", "王中南");
		data1.put("headImg", "http://www.baidu.com");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_第三方登陆, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}
}