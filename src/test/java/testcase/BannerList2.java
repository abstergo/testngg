package testcase;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import base.BaseTool;
import business.UserAccount;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：获取轮播图列表、轮播图跳转直播间请求接口
 * 参数：
 * */
public class BannerList2 {
	@BeforeMethod
	public void beforeMethod() {
		UserAccount.createUser(1);
	}

	@AfterMethod
	public void afterMethod() {
		UserAccount.delUser();
	}

	// 用例描述：获取轮播图列表，再根据ID跳转直播间
	@Test()
	public void BannerList2_case1() {
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_获取轮播图列表, new JSONObject());
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		assertTrue(respson1.getJSONArray("data").size() > 0);
		assertTrue(BaseTool.isURL(respson1.getJSONArray("data").getJSONObject(0).getString("forward")));
		assertTrue(respson1.getJSONArray("data").getJSONObject(0).getInt("id") > 0);
		assertEquals(respson1.getJSONArray("data").getJSONObject(0).getString("isShow"), "0");
		assertTrue(BaseTool.isDate(respson1.getJSONArray("data").getJSONObject(0).getString("liveEndTime")));
		assertEquals(respson1.getJSONArray("data").getJSONObject(0).getString("liveForward"), "0");
		assertTrue(BaseTool.isDate(respson1.getJSONArray("data").getJSONObject(0).getString("liveStartTime")));
		assertTrue(respson1.getJSONArray("data").getJSONObject(0).containsKey("livingRoomInfoVo"));
		assertTrue(respson1.getJSONArray("data").getJSONObject(0).getInt("messageId") >= 0);
		assertTrue(BaseTool.isDate(respson1.getJSONArray("data").getJSONObject(0).getString("showEndTime")));
		assertTrue(BaseTool.isDate(respson1.getJSONArray("data").getJSONObject(0).getString("showStartTime")));
		assertTrue(BaseTool.isURL(respson1.getJSONArray("data").getJSONObject(0).getString("source")));
		assertEquals(respson1.getJSONArray("data").getJSONObject(0).getString("target"), "1");
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONArray("data").getJSONObject(0).getString("title")));
		assertEquals(respson1.getJSONArray("data").getJSONObject(0).getString("userType"), "0");
		//
		String id = respson1.getJSONArray("data").getJSONObject(0).getString("id");
		//
		JSONObject data2 = new JSONObject();
		data2.put("id", id);
		JSONObject respson2 = BaseHttpClient.sendGetRequest(BaseData.URL_轮播图跳转直播间请求接口, data2);
		assertEquals(respson2.getString("code"), "1000");
		assertEquals(respson2.getString("msg"), "成功");
		assertTrue(BaseTool.isURL(respson2.getJSONObject("data").getString("forward")));
		assertEquals(respson2.getJSONObject("data").getString("isShow"), "0");
		assertTrue(BaseTool.isDate(respson2.getJSONObject("data").getString("liveEndTime")));
		assertEquals(respson2.getJSONObject("data").getString("liveForward"), "0");
		assertTrue(BaseTool.isDate(respson2.getJSONObject("data").getString("liveStartTime")));
		assertTrue(respson2.getJSONObject("data").containsKey("livingRoomInfoVo"));
		assertTrue(respson2.getJSONObject("data").getInt("messageId") >= 0);
		assertTrue(BaseTool.isDate(respson2.getJSONObject("data").getString("showEndTime")));
		assertTrue(BaseTool.isDate(respson2.getJSONObject("data").getString("showStartTime")));
		assertTrue(BaseTool.isURL(respson2.getJSONObject("data").getString("source")));
		assertEquals(respson2.getJSONObject("data").getString("target"), "1");
		assertTrue(BaseTool.isNotEmpty(respson2.getJSONObject("data").getString("title")));
		assertEquals(respson2.getJSONObject("data").getString("userType"), "0");
	}
}