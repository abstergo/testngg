package testcase;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import business.UserAccount;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：我的页面数据
 * 参数：
 * */
public class ZZZAssert_MyUserInfo {
	@BeforeMethod
	public void beforeMethod() {
		UserAccount.createUser(1);
	}

	@AfterMethod
	public void afterMethod() {
		UserAccount.delUser();
	}

	// 用例描述：正常
	@Test()
	public void MyUserInfo_case1() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_我的页面数据, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}
}