package testcase;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import base.BaseTool;
import business.UserAccount;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：申请主播获取开户行列表
 * 参数：
 * */
public class FindSysBankList {
	String count = (String) BaseTool.querySQL("select count(id) from sys_bank_info", "string", "count(id)");

	@BeforeMethod
	public void beforeMethod() {
		UserAccount.createUser(1);
	}

	@AfterMethod
	public void afterMethod() {
		UserAccount.delUser();
	}

	// 用例描述：正常
	@Test()
	public void FindSysBankList_case1() {
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_申请主播获取开户行列表, new JSONObject());
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		assertEquals(respson1.getJSONArray("data").size() + "", count);
		assertTrue(respson1.getJSONArray("data").getJSONObject(0).getInt("id") > 0);
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONArray("data").getJSONObject(0).getString("bank")));
	}
}