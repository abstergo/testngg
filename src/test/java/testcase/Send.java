package testcase;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import business.UserAccount;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：发送短信验证码
 * 参数：smsType(1为登陆手机验证码、2为申请主播认证发送验证码、3为变更或绑定手机号验证码)
 * */
public class Send {
	@BeforeMethod
	public void beforeMethod() {
		UserAccount.createUser(1);
	}

	@AfterMethod
	public void afterMethod() {
		UserAccount.delUser();
	}

	// 用例描述：登陆手机验证码
	@Test()
	public void Send_case1() {
		JSONObject data1 = new JSONObject();
		data1.put("smsType", "1");
		data1.put("mobile", UserAccount.userId.get(0));
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_发送短信验证码, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：申请主播认证发送验证码
	@Test()
	public void Send_case2() {
		JSONObject data1 = new JSONObject();
		data1.put("smsType", "2");
		data1.put("mobile", UserAccount.userId.get(0));
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_发送短信验证码, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：变更或绑定手机号验证码
	@Test()
	public void Send_case3() {
		JSONObject data1 = new JSONObject();
		data1.put("smsType", "3");
		data1.put("mobile", UserAccount.userId.get(0));
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_发送短信验证码, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}
}