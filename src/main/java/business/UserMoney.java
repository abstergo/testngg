package business;

import base.BaseTool;
import redis.clients.jedis.Jedis;

public class UserMoney {
	/** 普通用户与主播用户饭票：update web_user set balance=999 where user_id=1673260 */
	public static void balanceUpdate(String userId, String balance) {
		BaseTool.executeSQL("update web_user set balance=" + balance + " where user_id=" + userId);
	}

	/** 普通用户与主播用户盒饭累计：set userHefanTotal_1673260 999 */
	public static void balanceSumUpdate(String userId, String ticket) {
		Jedis jedis = BaseTool.connectRedis();
		jedis.set("userHefanTotal_" + userId, ticket);
	}

	/** 普通用户盒饭：insert into web_user_account (`user_id`, `income`, `create_time`, `update_time`) values ('1680272', '123', now(), now()) */
	public static void userHefanInsert(String userId, String ticket) {
		BaseTool.executeSQL("insert into web_user_account (`user_id`, `income`, `create_time`, `update_time`) values ('" + userId + "', '" + ticket + "', now(), now())");
	}

	/** 普通用户盒饭：update web_user_account set income=456 where user_id=1680272 */
	public static void userHefanUpdate(String userId, String ticket) {
		BaseTool.executeSQL("update web_user_account set income=" + ticket + " where user_id=" + userId);
	}

	/** 主播用户盒饭：insert into anchor_ticket_month (`user_id`, `real_ticket`, `false_ticket`, `pay_ticket`, `years_month`, `create_date`, `update_date`, `delete_flag`) values ('1673261', '456', '0', '0', '201708', now(), now(), '0') */
	public static void presenterHefanInsert(String userId, String ticket) {
		String month = BaseTool.getCurrentDate("yyyyMM");
		BaseTool.executeSQL("insert into anchor_ticket_month (`user_id`, `real_ticket`, `false_ticket`, `pay_ticket`, `years_month`, `create_date`, `update_date`, `delete_flag`) values ('" + userId + "', '" + ticket + "', '0', '0', '" + month + "', now(), now(), '0')");
	}

	/** 主播用户盒饭：update anchor_ticket_month set real_ticket=999 where user_id=1673261 */
	public static void presenterHefanUpdate(String userId, String ticket) {
		BaseTool.executeSQL("update anchor_ticket_month set false_ticket=0" + " where user_id=" + userId);
		BaseTool.executeSQL("update anchor_ticket_month set pay_ticket=0" + " where user_id=" + userId);
		BaseTool.executeSQL("update anchor_ticket_month set real_ticket=" + ticket + " where user_id=" + userId);
	}

	public static void main(String[] args) {
		balanceUpdate("1673260", "911");
		balanceSumUpdate("1673260", "912");
		userHefanInsert("1680273", "913");
		userHefanUpdate("1680273", "914");
		presenterHefanInsert("1673261", "915");
		presenterHefanUpdate("1673261", "916");
	}
}