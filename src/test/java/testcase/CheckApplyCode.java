package testcase;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import business.UserAccount;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：验证申请主播时填写的申请码是否正确
 * 参数：
 * */
public class CheckApplyCode {
	@BeforeMethod
	public void beforeMethod() {
		UserAccount.createUser(1);
	}

	@AfterMethod
	public void afterMethod() {
		UserAccount.delUser();
	}

	// 用例描述：正确申请码
	@Test()
	public void CheckApplyCode_case1() {
		JSONObject data1 = new JSONObject();
		data1.put("applyCode", "ZhiDongHua999");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_验证申请主播时填写的申请码是否正确, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：错误申请码
	@Test()
	public void CheckApplyCode_case2() {
		JSONObject data1 = new JSONObject();
		data1.put("applyCode", "ZhiDongHua");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_验证申请主播时填写的申请码是否正确, data1);
		assertEquals(respson1.getString("code"), "4011");
		assertEquals(respson1.getString("msg"), "申请主播申请码无效");
	}

	// 用例描述：空申请码
	@Test()
	public void CheckApplyCode_case3() {
		JSONObject data1 = new JSONObject();
		data1.put("applyCode", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_验证申请主播时填写的申请码是否正确, data1);
		assertEquals(respson1.getString("code"), "4011");
		assertEquals(respson1.getString("msg"), "申请主播申请码无效");
	}
}