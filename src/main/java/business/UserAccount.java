package business;

import java.util.ArrayList;

import base.BaseHttpClient;
import base.BaseTool;
import redis.clients.jedis.Jedis;

public class UserAccount {
	public static ArrayList<String> mobile;
	public static ArrayList<String> userId;

	/** 创建新用户，获取mobile与userId */
	public static void createUser(int userCount) {
		mobile = new ArrayList<String>();
		userId = new ArrayList<String>();

		for (int i = 0; i < userCount; i++) {
			BaseTool.sleepWait(1);
			mobile.add("999" + BaseTool.getStringByTime(8));

			BaseHttpClient.loginPhone(mobile.get(i));
			userId.add((String) BaseTool.querySQL("select user_id from web_user where mobile=" + mobile.get(i), "string", "user_id"));
		}
	}

	/** 删除用户 */
	public static void delUser() {
		if (userId.size() > 0) {
			for (int i = 0; i < userId.size(); i++) {
				BaseTool.executeSQL("update web_user set is_del=1 where user_id=" + userId.get(i));

				Jedis jedis = BaseTool.connectRedis();
				jedis.del("userInfo_" + userId.get(i));
			}
		}
	}

	/** 删除用户 */
	public static void delUserByUerID(String user_id) {
		BaseTool.executeSQL("update web_user set is_del=1 where user_id=" + user_id);

		Jedis jedis = BaseTool.connectRedis();
		jedis.del("userInfo_" + user_id);
	}
}