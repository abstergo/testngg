package testcase;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import business.UserAccount;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：申请主播认证提交
 * 参数：userId、realName姓名、history是否有过网络主播经历(0无、1有)、platForm以往的直播平台、liveId以往的直播ID
 * 参数：applyCode有无申请码、msCode短信验证码、credentialType证件类型(1身份证、2护照、3通行证)
 * 参数：personId身份证号/护照号/通行证号、area地区/国籍、salaryPayType工资支付方式(0银行卡、1支付宝)
 * 参数：bankName开户行、bankBranchName开户支行、bankAccount持卡人姓名、bankId银行卡号
 * 参数：contactPhone联系电话、alipayName支付宝姓名、alipayNo支付宝账号
 * 参数：lifePhone1生活照、lifePhone2生活照、lifePhone3生活照
 * 参数：personImgByHand手持身份证/护照/通行证照片、personImgHeads身份证/护照/通行证正面照、personImgTails身份证/护照/通行证背面照
 * */
public class UserIdentitySubmit {
	@BeforeMethod
	public void beforeMethod() {
		UserAccount.createUser(1);
	}

	@AfterMethod
	public void afterMethod() {
		UserAccount.delUser();
	}

	// 用例描述：无网络主播经历+无申请码+身份证+支付宝
	@Test()
	public void UserIdentitySubmit_case1() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("realName", "王中南");
		data1.put("history", "0");
		data1.put("credentialType", "1");
		data1.put("personId", "110105198410205017");
		data1.put("salaryPayType", "1");
		data1.put("alipayName", "王中南");
		data1.put("alipayNo", "wanghzongnan84@");
		data1.put("contactPhone", "13426342883");
		data1.put("msCode", "1111");
		data1.put("lifePhone1", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		data1.put("lifePhone2", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		data1.put("lifePhone3", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		data1.put("personImgByHand", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		data1.put("personImgHeads", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		data1.put("personImgTails", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_申请主播认证提交, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：有网络主播经历+有申请码+通行证+银行卡
	@Test()
	public void UserIdentitySubmit_case2() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("realName", "王中南");
		data1.put("history", "1");
		data1.put("platForm", "熊猫");
		data1.put("liveId", "999888777");
		data1.put("applyCode", "ZhiDongHua999");
		data1.put("credentialType", "3");
		data1.put("area", "香港");
		data1.put("personId", "110105198410205017");
		data1.put("salaryPayType", "0");
		data1.put("bankName", "中国银行");
		data1.put("bankBranchName", "中国银行支行");
		data1.put("bankAccount", "王中南");
		data1.put("bankId", "6214830139957999");
		data1.put("contactPhone", "13426342883");
		data1.put("msCode", "1111");
		data1.put("lifePhone1", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		data1.put("lifePhone2", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		data1.put("lifePhone3", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		data1.put("personImgByHand", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		data1.put("personImgHeads", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		data1.put("personImgTails", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_申请主播认证提交, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：有网络主播经历+有申请码+护照+银行卡
	@Test()
	public void UserIdentitySubmit_case3() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("realName", "王中南");
		data1.put("history", "1");
		data1.put("platForm", "熊猫");
		data1.put("liveId", "999888777");
		data1.put("applyCode", "ZhiDongHua999");
		data1.put("credentialType", "2");
		data1.put("area", "非洲");
		data1.put("personId", "110105198410205017");
		data1.put("salaryPayType", "0");
		data1.put("bankName", "中国银行");
		data1.put("bankBranchName", "中国银行支行");
		data1.put("bankAccount", "王中南");
		data1.put("bankId", "6214830139957999");
		data1.put("contactPhone", "13426342883");
		data1.put("msCode", "1111");
		data1.put("lifePhone1", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		data1.put("lifePhone2", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		data1.put("lifePhone3", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		data1.put("personImgByHand", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		data1.put("personImgHeads", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		data1.put("personImgTails", "http://img.hefantv.com/20170731/ef2e5791a9b54c94b9c7f3f071b455c7.jpg");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_申请主播认证提交, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}
}