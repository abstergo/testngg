package testcase;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import base.BaseTool;
import business.UserAccount;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：app支付
 * 参数：paymentType支付类型(1为IOS的APPLE PAY、2为Android的微信支付、3为Android的支付宝支付)
 * */
public class AppPayOrder {
	@BeforeMethod
	public void beforeMethod() {
		UserAccount.createUser(1);
	}

	@AfterMethod
	public void afterMethod() {
		UserAccount.delUser();
	}

	// ios+产品ID+无赠送
	@Test()
	public void AppPayOrder_case11() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "43");
		data1.put("paymentType", "1");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		assertEquals(respson1.getJSONObject("data").getString("income"), "686");
		assertEquals(respson1.getJSONObject("data").getString("rewardFanpiao"), "0");
		assertEquals(respson1.getJSONObject("data").getString("incomeAmount"), "686");
		assertEquals(respson1.getJSONObject("data").getDouble("payAmount"), 98.00);
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("orderId")));
	}

	// ios+产品ID+有赠送
	@Test()
	public void AppPayOrder_case12() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "42");
		data1.put("paymentType", "1");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		assertEquals(respson1.getJSONObject("data").getString("income"), "2156");
		assertEquals(respson1.getJSONObject("data").getString("rewardFanpiao"), "539");
		assertEquals(respson1.getJSONObject("data").getString("incomeAmount"), "2695");
		assertEquals(respson1.getJSONObject("data").getDouble("payAmount"), 308.00);
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("orderId")));
	}

	// ios+无效产品ID
	@Test()
	public void AppPayOrder_case13() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "000");
		data1.put("paymentType", "1");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "参数验证错误");
	}

	// android微信+产品ID+无赠送
	@Test()
	public void AppPayOrder_case21() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "46");
		data1.put("fanpiao", "0");
		data1.put("paymentType", "2");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		assertEquals(respson1.getJSONObject("data").getString("income"), "300");
		assertEquals(respson1.getJSONObject("data").getString("rewardFanpiao"), "0");
		assertEquals(respson1.getJSONObject("data").getString("incomeAmount"), "300");
		assertEquals(respson1.getJSONObject("data").getDouble("payAmount"), 30.00);
		assertEquals(respson1.getJSONObject("data").getString("package"), "Sign=WXPay");
		assertEquals(respson1.getJSONObject("data").getString("packageValue"), "Sign=WXPay");
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("sign")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("noncestr")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("appid")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("partnerid")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("prepayid")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("timestamp")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("orderId")));
	}

	// android微信+产品ID+有赠送
	@Test()
	public void AppPayOrder_case22() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "47");
		data1.put("fanpiao", "0");
		data1.put("paymentType", "2");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		assertEquals(respson1.getJSONObject("data").getString("income"), "980");
		assertEquals(respson1.getJSONObject("data").getString("rewardFanpiao"), "10");
		assertEquals(respson1.getJSONObject("data").getString("incomeAmount"), "990");
		assertEquals(respson1.getJSONObject("data").getDouble("payAmount"), 1.00);
		assertEquals(respson1.getJSONObject("data").getString("package"), "Sign=WXPay");
		assertEquals(respson1.getJSONObject("data").getString("packageValue"), "Sign=WXPay");
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("sign")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("noncestr")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("appid")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("partnerid")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("prepayid")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("timestamp")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("orderId")));
	}

	// android微信+自定义ID
	@Test()
	public void AppPayOrder_case23() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "0");
		data1.put("fanpiao", "20");
		data1.put("paymentType", "2");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		assertEquals(respson1.getJSONObject("data").getString("income"), "20");
		assertEquals(respson1.getJSONObject("data").getString("rewardFanpiao"), "0");
		assertEquals(respson1.getJSONObject("data").getString("incomeAmount"), "20");
		assertEquals(respson1.getJSONObject("data").getDouble("payAmount"), 2.00);
		assertEquals(respson1.getJSONObject("data").getString("package"), "Sign=WXPay");
		assertEquals(respson1.getJSONObject("data").getString("packageValue"), "Sign=WXPay");
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("sign")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("noncestr")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("appid")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("partnerid")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("prepayid")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("timestamp")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("orderId")));
	}

	// android微信+自定义ID+与有赠送产品饭票数相同
	@Test()
	public void AppPayOrder_case24() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "0");
		data1.put("fanpiao", "980");
		data1.put("paymentType", "2");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		assertEquals(respson1.getJSONObject("data").getString("income"), "980");
		assertEquals(respson1.getJSONObject("data").getString("rewardFanpiao"), "0");
		assertEquals(respson1.getJSONObject("data").getString("incomeAmount"), "980");
		assertEquals(respson1.getJSONObject("data").getDouble("payAmount"), 98.00);
		assertEquals(respson1.getJSONObject("data").getString("package"), "Sign=WXPay");
		assertEquals(respson1.getJSONObject("data").getString("packageValue"), "Sign=WXPay");
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("sign")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("noncestr")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("appid")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("partnerid")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("prepayid")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("timestamp")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("orderId")));
	}

	// android微信+自定义ID+9饭票
	@Test()
	public void AppPayOrder_case25() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "0");
		data1.put("fanpiao", "9");
		data1.put("paymentType", "2");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "4504");
		assertEquals(respson1.getString("msg"), "支付金额不符合额度限制");
	}

	// android微信+自定义ID+12.3饭票
	@Test()
	public void AppPayOrder_case26() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "0");
		data1.put("fanpiao", "12.3");
		data1.put("paymentType", "2");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "2001");
		assertEquals(respson1.getString("msg"), "未知异常");
	}

	// android微信+自定义ID+负数10饭票
	@Test()
	public void AppPayOrder_case27() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "0");
		data1.put("fanpiao", "-10");
		data1.put("paymentType", "2");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "4504");
		assertEquals(respson1.getString("msg"), "支付金额不符合额度限制");
	}

	// android微信+自定义ID+中文10饭票
	@Test()
	public void AppPayOrder_case28() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "0");
		data1.put("fanpiao", "中文10");
		data1.put("paymentType", "2");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "2001");
		assertEquals(respson1.getString("msg"), "未知异常");
	}

	// android支付宝+产品ID+无赠送
	@Test()
	public void AppPayOrder_case31() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "46");
		data1.put("fanpiao", "0");
		data1.put("paymentType", "3");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		assertEquals(respson1.getJSONObject("data").getString("income"), "300");
		assertEquals(respson1.getJSONObject("data").getString("rewardFanpiao"), "0");
		assertEquals(respson1.getJSONObject("data").getString("incomeAmount"), "300");
		assertEquals(respson1.getJSONObject("data").getDouble("payAmount"), 30.00);
		assertTrue(respson1.getJSONObject("data").getString("reqPayUrl").contains("app_id=2016092201948315"));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("orderId")));
	}

	// android支付宝+产品ID+有赠送
	@Test()
	public void AppPayOrder_case32() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "47");
		data1.put("fanpiao", "0");
		data1.put("paymentType", "3");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		assertEquals(respson1.getJSONObject("data").getString("income"), "980");
		assertEquals(respson1.getJSONObject("data").getString("rewardFanpiao"), "10");
		assertEquals(respson1.getJSONObject("data").getString("incomeAmount"), "990");
		assertEquals(respson1.getJSONObject("data").getDouble("payAmount"), 1.00);
		assertTrue(respson1.getJSONObject("data").getString("reqPayUrl").contains("app_id=2016092201948315"));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("orderId")));
	}

	// android支付宝+自定义ID
	@Test()
	public void AppPayOrder_case33() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "0");
		data1.put("fanpiao", "20");
		data1.put("paymentType", "3");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		assertEquals(respson1.getJSONObject("data").getString("income"), "20");
		assertEquals(respson1.getJSONObject("data").getString("rewardFanpiao"), "0");
		assertEquals(respson1.getJSONObject("data").getString("incomeAmount"), "20");
		assertEquals(respson1.getJSONObject("data").getDouble("payAmount"), 2.00);
		assertTrue(respson1.getJSONObject("data").getString("reqPayUrl").contains("app_id=2016092201948315"));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("orderId")));
	}

	// android支付宝+自定义ID+与有赠送产品饭票数相同
	@Test()
	public void AppPayOrder_case34() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "0");
		data1.put("fanpiao", "980");
		data1.put("paymentType", "3");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		assertEquals(respson1.getJSONObject("data").getString("income"), "980");
		assertEquals(respson1.getJSONObject("data").getString("rewardFanpiao"), "0");
		assertEquals(respson1.getJSONObject("data").getString("incomeAmount"), "980");
		assertEquals(respson1.getJSONObject("data").getDouble("payAmount"), 98.00);
		assertTrue(respson1.getJSONObject("data").getString("reqPayUrl").contains("app_id=2016092201948315"));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("orderId")));
	}

	// android支付宝+自定义ID+9饭票
	@Test()
	public void AppPayOrder_case35() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "0");
		data1.put("fanpiao", "9");
		data1.put("paymentType", "3");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "4504");
		assertEquals(respson1.getString("msg"), "支付金额不符合额度限制");
	}

	// android支付宝+自定义ID+12.3饭票
	@Test()
	public void AppPayOrder_case36() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "0");
		data1.put("fanpiao", "12.3");
		data1.put("paymentType", "3");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "2001");
		assertEquals(respson1.getString("msg"), "未知异常");
	}

	// android支付宝+自定义ID+负数10饭票
	@Test()
	public void AppPayOrder_case37() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "0");
		data1.put("fanpiao", "-10");
		data1.put("paymentType", "3");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "4504");
		assertEquals(respson1.getString("msg"), "支付金额不符合额度限制");
	}

	// android支付宝+自定义ID+中文10饭票
	@Test()
	public void AppPayOrder_case38() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("id", "0");
		data1.put("fanpiao", "中文10");
		data1.put("paymentType", "3");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_app支付, data1);
		assertEquals(respson1.getString("code"), "2001");
		assertEquals(respson1.getString("msg"), "未知异常");
	}
}