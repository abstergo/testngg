package testcase;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import base.BaseTool;
import business.BusinessData;
import business.UserAccount;
import business.UserMoney;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：盒饭兑换
 * 参数：hefanNum盒饭数、fanpiaoNum饭票数
 * */
public class DoMealTicketExchange {
	@BeforeMethod
	public void beforeMethod() {
		UserAccount.createUser(1);
	}

	@AfterMethod
	public void afterMethod() {
		UserAccount.delUser();
	}

	// 用例描述：普通用户--1000盒饭时，200盒饭兑换100饭票，检查剩余盒饭数、饭票数
	@Test()
	public void DoMealTicketExchange_case101() {
		UserMoney.userHefanInsert(UserAccount.userId.get(0), "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("hefanNum", "200");
		data1.put("fanpiaoNum", "100");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + UserAccount.userId.get(0), "string", "balance");
		String income = (String) BaseTool.querySQL("select income from web_user_account where user_id=" + UserAccount.userId.get(0), "string", "income");
		assertEquals(balance, "100");
		assertEquals(income, "800");
	}

	// 用例描述：普通用户--1000盒饭时，2000盒饭兑换1000饭票
	@Test()
	public void DoMealTicketExchange_case102() {
		UserMoney.userHefanInsert(UserAccount.userId.get(0), "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("hefanNum", "2000");
		data1.put("fanpiaoNum", "1000");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3101");
		assertEquals(respson1.getString("msg"), "可兑换盒饭数不足");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + UserAccount.userId.get(0), "string", "balance");
		String income = (String) BaseTool.querySQL("select income from web_user_account where user_id=" + UserAccount.userId.get(0), "string", "income");
		assertEquals(balance, "0");
		assertEquals(income, "1000");
	}

	// 用例描述：普通用户--1000盒饭时，10盒饭兑换5饭票
	@Test()
	public void DoMealTicketExchange_case103() {
		UserMoney.userHefanInsert(UserAccount.userId.get(0), "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("hefanNum", "10");
		data1.put("fanpiaoNum", "5");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "饭票数应为100的倍数");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + UserAccount.userId.get(0), "string", "balance");
		String income = (String) BaseTool.querySQL("select income from web_user_account where user_id=" + UserAccount.userId.get(0), "string", "income");
		assertEquals(balance, "0");
		assertEquals(income, "1000");
	}

	// 用例描述：普通用户--1000盒饭时，222盒饭兑换111饭票
	@Test()
	public void DoMealTicketExchange_case104() {
		UserMoney.userHefanInsert(UserAccount.userId.get(0), "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("hefanNum", "222");
		data1.put("fanpiaoNum", "111");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "饭票数应为100的倍数");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + UserAccount.userId.get(0), "string", "balance");
		String income = (String) BaseTool.querySQL("select income from web_user_account where user_id=" + UserAccount.userId.get(0), "string", "income");
		assertEquals(balance, "0");
		assertEquals(income, "1000");
	}

	// 用例描述：普通用户--1000盒饭时，200.0盒饭兑换100.0饭票
	@Test()
	public void DoMealTicketExchange_case105() {
		UserMoney.userHefanInsert(UserAccount.userId.get(0), "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("hefanNum", "200.0");
		data1.put("fanpiaoNum", "100.0");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "兑换饭票数不能为0");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + UserAccount.userId.get(0), "string", "balance");
		String income = (String) BaseTool.querySQL("select income from web_user_account where user_id=" + UserAccount.userId.get(0), "string", "income");
		assertEquals(balance, "0");
		assertEquals(income, "1000");
	}

	// 用例描述：普通用户--1000盒饭时，-200盒饭兑换-100饭票
	@Test()
	public void DoMealTicketExchange_case106() {
		UserMoney.userHefanInsert(UserAccount.userId.get(0), "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("hefanNum", "-200");
		data1.put("fanpiaoNum", "-100");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "兑换饭票数不能为0");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + UserAccount.userId.get(0), "string", "balance");
		String income = (String) BaseTool.querySQL("select income from web_user_account where user_id=" + UserAccount.userId.get(0), "string", "income");
		assertEquals(balance, "0");
		assertEquals(income, "1000");
	}

	// 用例描述：普通用户--1000盒饭时，"中国"盒饭兑换"中国"饭票
	@Test()
	public void DoMealTicketExchange_case107() {
		UserMoney.userHefanInsert(UserAccount.userId.get(0), "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("hefanNum", "中国");
		data1.put("fanpiaoNum", "中国");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "兑换饭票数不能为0");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + UserAccount.userId.get(0), "string", "balance");
		String income = (String) BaseTool.querySQL("select income from web_user_account where user_id=" + UserAccount.userId.get(0), "string", "income");
		assertEquals(balance, "0");
		assertEquals(income, "1000");
	}

	// 用例描述：普通用户--1000盒饭时，0盒饭兑换0饭票
	@Test()
	public void DoMealTicketExchange_case108() {
		UserMoney.userHefanInsert(UserAccount.userId.get(0), "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("hefanNum", "0");
		data1.put("fanpiaoNum", "0");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "兑换饭票数不能为0");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + UserAccount.userId.get(0), "string", "balance");
		String income = (String) BaseTool.querySQL("select income from web_user_account where user_id=" + UserAccount.userId.get(0), "string", "income");
		assertEquals(balance, "0");
		assertEquals(income, "1000");
	}

	// 用例描述：普通用户--1000盒饭时，空盒饭兑换空饭票
	@Test()
	public void DoMealTicketExchange_case109() {
		UserMoney.userHefanInsert(UserAccount.userId.get(0), "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("hefanNum", "");
		data1.put("fanpiaoNum", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "兑换饭票数不能为0");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + UserAccount.userId.get(0), "string", "balance");
		String income = (String) BaseTool.querySQL("select income from web_user_account where user_id=" + UserAccount.userId.get(0), "string", "income");
		assertEquals(balance, "0");
		assertEquals(income, "1000");
	}

	// 用例描述：普通用户--0盒饭时，200盒饭兑换100饭票
	@Test()
	public void DoMealTicketExchange_case110() {
		UserMoney.userHefanInsert(UserAccount.userId.get(0), "0");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("hefanNum", "200");
		data1.put("fanpiaoNum", "100");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3101");
		assertEquals(respson1.getString("msg"), "可兑换盒饭数不足");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + UserAccount.userId.get(0), "string", "balance");
		String income = (String) BaseTool.querySQL("select income from web_user_account where user_id=" + UserAccount.userId.get(0), "string", "income");
		assertEquals(balance, "0");
		assertEquals(income, "0");
	}

	// 用例描述：主播用户--1000盒饭时，200盒饭兑换100饭票，检查剩余盒饭数、饭票数
	@Test()
	public void DoMealTicketExchange_case201() {
		UserMoney.balanceUpdate(BusinessData.ZhuBo1_UserId, "0");
		UserMoney.presenterHefanUpdate(BusinessData.ZhuBo1_UserId, "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", BusinessData.ZhuBo1_UserId);
		data1.put("hefanNum", "200");
		data1.put("fanpiaoNum", "100");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + BusinessData.ZhuBo1_UserId, "string", "balance");
		int real_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select real_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "real_ticket"));
		int pay_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select pay_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "pay_ticket"));
		assertEquals(balance, "100");
		assertEquals((real_ticket - pay_ticket) + "", "800");
	}

	// 用例描述：主播用户--1000盒饭时，2000盒饭兑换1000饭票
	@Test()
	public void DoMealTicketExchange_case202() {
		UserMoney.balanceUpdate(BusinessData.ZhuBo1_UserId, "0");
		UserMoney.presenterHefanUpdate(BusinessData.ZhuBo1_UserId, "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", BusinessData.ZhuBo1_UserId);
		data1.put("hefanNum", "2000");
		data1.put("fanpiaoNum", "1000");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3101");
		assertEquals(respson1.getString("msg"), "可兑换盒饭数不足");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + BusinessData.ZhuBo1_UserId, "string", "balance");
		int real_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select real_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "real_ticket"));
		int pay_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select pay_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "pay_ticket"));
		assertEquals(balance, "0");
		assertEquals((real_ticket - pay_ticket) + "", "1000");
	}

	// 用例描述：主播用户--1000盒饭时，10盒饭兑换5饭票
	@Test()
	public void DoMealTicketExchange_case203() {
		UserMoney.balanceUpdate(BusinessData.ZhuBo1_UserId, "0");
		UserMoney.presenterHefanUpdate(BusinessData.ZhuBo1_UserId, "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", BusinessData.ZhuBo1_UserId);
		data1.put("hefanNum", "10");
		data1.put("fanpiaoNum", "5");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "饭票数应为100的倍数");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + BusinessData.ZhuBo1_UserId, "string", "balance");
		int real_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select real_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "real_ticket"));
		int pay_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select pay_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "pay_ticket"));
		assertEquals(balance, "0");
		assertEquals((real_ticket - pay_ticket) + "", "1000");
	}

	// 用例描述：主播用户--1000盒饭时，222盒饭兑换111饭票
	@Test()
	public void DoMealTicketExchange_case204() {
		UserMoney.balanceUpdate(BusinessData.ZhuBo1_UserId, "0");
		UserMoney.presenterHefanUpdate(BusinessData.ZhuBo1_UserId, "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", BusinessData.ZhuBo1_UserId);
		data1.put("hefanNum", "222");
		data1.put("fanpiaoNum", "111");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "饭票数应为100的倍数");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + BusinessData.ZhuBo1_UserId, "string", "balance");
		int real_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select real_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "real_ticket"));
		int pay_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select pay_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "pay_ticket"));
		assertEquals(balance, "0");
		assertEquals((real_ticket - pay_ticket) + "", "1000");
	}

	// 用例描述：主播用户--1000盒饭时，200.0盒饭兑换100.0饭票
	@Test()
	public void DoMealTicketExchange_case205() {
		UserMoney.balanceUpdate(BusinessData.ZhuBo1_UserId, "0");
		UserMoney.presenterHefanUpdate(BusinessData.ZhuBo1_UserId, "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", BusinessData.ZhuBo1_UserId);
		data1.put("hefanNum", "200.0");
		data1.put("fanpiaoNum", "100.0");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "兑换饭票数不能为0");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + BusinessData.ZhuBo1_UserId, "string", "balance");
		int real_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select real_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "real_ticket"));
		int pay_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select pay_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "pay_ticket"));
		assertEquals(balance, "0");
		assertEquals((real_ticket - pay_ticket) + "", "1000");
	}

	// 用例描述：主播用户--1000盒饭时，-200盒饭兑换-100饭票
	@Test()
	public void DoMealTicketExchange_case206() {
		UserMoney.balanceUpdate(BusinessData.ZhuBo1_UserId, "0");
		UserMoney.presenterHefanUpdate(BusinessData.ZhuBo1_UserId, "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", BusinessData.ZhuBo1_UserId);
		data1.put("hefanNum", "-200");
		data1.put("fanpiaoNum", "-100");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "兑换饭票数不能为0");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + BusinessData.ZhuBo1_UserId, "string", "balance");
		int real_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select real_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "real_ticket"));
		int pay_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select pay_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "pay_ticket"));
		assertEquals(balance, "0");
		assertEquals((real_ticket - pay_ticket) + "", "1000");
	}

	// 用例描述：主播用户--1000盒饭时，"中国"盒饭兑换"中国"饭票
	@Test()
	public void DoMealTicketExchange_case207() {
		UserMoney.balanceUpdate(BusinessData.ZhuBo1_UserId, "0");
		UserMoney.presenterHefanUpdate(BusinessData.ZhuBo1_UserId, "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", BusinessData.ZhuBo1_UserId);
		data1.put("hefanNum", "中国");
		data1.put("fanpiaoNum", "中国");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "兑换饭票数不能为0");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + BusinessData.ZhuBo1_UserId, "string", "balance");
		int real_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select real_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "real_ticket"));
		int pay_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select pay_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "pay_ticket"));
		assertEquals(balance, "0");
		assertEquals((real_ticket - pay_ticket) + "", "1000");
	}

	// 用例描述：主播用户--1000盒饭时，0盒饭兑换0饭票
	@Test()
	public void DoMealTicketExchange_case208() {
		UserMoney.balanceUpdate(BusinessData.ZhuBo1_UserId, "0");
		UserMoney.presenterHefanUpdate(BusinessData.ZhuBo1_UserId, "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", BusinessData.ZhuBo1_UserId);
		data1.put("hefanNum", "0");
		data1.put("fanpiaoNum", "0");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "兑换饭票数不能为0");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + BusinessData.ZhuBo1_UserId, "string", "balance");
		int real_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select real_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "real_ticket"));
		int pay_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select pay_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "pay_ticket"));
		assertEquals(balance, "0");
		assertEquals((real_ticket - pay_ticket) + "", "1000");
	}

	// 用例描述：主播用户--1000盒饭时，空盒饭兑换空饭票
	@Test()
	public void DoMealTicketExchange_case209() {
		UserMoney.balanceUpdate(BusinessData.ZhuBo1_UserId, "0");
		UserMoney.presenterHefanUpdate(BusinessData.ZhuBo1_UserId, "1000");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", BusinessData.ZhuBo1_UserId);
		data1.put("hefanNum", "");
		data1.put("fanpiaoNum", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "兑换饭票数不能为0");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + BusinessData.ZhuBo1_UserId, "string", "balance");
		int real_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select real_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "real_ticket"));
		int pay_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select pay_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "pay_ticket"));
		assertEquals(balance, "0");
		assertEquals((real_ticket - pay_ticket) + "", "1000");
	}

	// 用例描述：主播用户--0盒饭时，200盒饭兑换100饭票
	@Test()
	public void DoMealTicketExchange_case210() {
		UserMoney.balanceUpdate(BusinessData.ZhuBo1_UserId, "0");
		UserMoney.presenterHefanUpdate(BusinessData.ZhuBo1_UserId, "0");
		//
		JSONObject data1 = new JSONObject();
		data1.put("userId", BusinessData.ZhuBo1_UserId);
		data1.put("hefanNum", "200");
		data1.put("fanpiaoNum", "100");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_盒饭兑换, data1);
		assertEquals(respson1.getString("code"), "3101");
		assertEquals(respson1.getString("msg"), "可兑换盒饭数不足");
		// 检查数据库饭票数、盒饭数
		String balance = (String) BaseTool.querySQL("select balance from web_user where user_id=" + BusinessData.ZhuBo1_UserId, "string", "balance");
		int real_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select real_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "real_ticket"));
		int pay_ticket = BaseTool.stringToInt((String) BaseTool.querySQL("select pay_ticket from anchor_ticket_month where user_id=" + BusinessData.ZhuBo1_UserId, "string", "pay_ticket"));
		assertEquals(balance, "0");
		assertEquals((real_ticket - pay_ticket) + "", "0");
	}
}