package testcase;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import business.UserAccount;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：编辑用户基本信息
 * 参数：
 * */
public class EditUserInfo {
	@BeforeMethod
	public void beforeMethod() {
		UserAccount.createUser(1);
	}

	@AfterMethod
	public void afterMethod() {
		UserAccount.delUser();
	}

	// 用例描述：编辑用户头像
	@Test()
	public void EditUserInfo_case11() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("headImg", "http://img.hefantv.com/20170801/a413903b6add4c95a0d0bb6902a96f83.jpg");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_编辑用户基本信息, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：编辑用户头像--空
	@Test()
	public void EditUserInfo_case12() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("headImg", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_编辑用户基本信息, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "参数验证错误");
	}

	// 用例描述：编辑用户昵称
	@Test()
	public void EditUserInfo_case21() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("nickName", "wangzn");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_编辑用户基本信息, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：编辑用户昵称--空
	@Test()
	public void EditUserInfo_case22() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("nickName", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_编辑用户基本信息, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "参数验证错误");
	}

	// 用例描述：编辑用户性别
	@Test()
	public void EditUserInfo_case31() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("sex", 1);
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_编辑用户基本信息, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：编辑用户性别--空
	@Test()
	public void EditUserInfo_case32() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("sex", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_编辑用户基本信息, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "参数验证错误");
	}

	// 用例描述：编辑用户个性签名
	@Test()
	public void EditUserInfo_case41() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("personSign", "个性签名");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_编辑用户基本信息, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：编辑用户个性签名--空
	@Test()
	public void EditUserInfo_case42() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("personSign", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_编辑用户基本信息, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：编辑用户年龄
	@Test()
	public void EditUserInfo_case51() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("age", "33");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_编辑用户基本信息, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：编辑用户年龄--空
	@Test()
	public void EditUserInfo_case52() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("age", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_编辑用户基本信息, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "参数验证错误");
	}

	// 用例描述：编辑用户情感状况
	@Test()
	public void EditUserInfo_case61() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("feeling", "已婚");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_编辑用户基本信息, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：编辑用户情感状况--空
	@Test()
	public void EditUserInfo_case62() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("feeling", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_编辑用户基本信息, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "参数验证错误");
	}

	// 用例描述：编辑用户家乡
	@Test()
	public void EditUserInfo_case71() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("province", "北京市");
		data1.put("city", "北京市");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_编辑用户基本信息, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：编辑用户家乡--空
	@Test()
	public void EditUserInfo_case72() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("province", "");
		data1.put("city", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_编辑用户基本信息, data1);
		assertEquals(respson1.getString("code"), "3000");
		assertEquals(respson1.getString("msg"), "参数验证错误");
	}

	// 用例描述：编辑用户职业
	@Test()
	public void EditUserInfo_case81() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("job", "互联网");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_编辑用户基本信息, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：编辑用户职业--空
	@Test()
	public void EditUserInfo_case82() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		data1.put("job", "");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_编辑用户基本信息, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}
}