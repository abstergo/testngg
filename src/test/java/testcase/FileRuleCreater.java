package testcase;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import base.BaseTool;
import business.UserAccount;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：oss上传文件规则获取
 * 参数：createNum创建数量、type(0图片文件、1视频文件)
 * */
public class FileRuleCreater {
	@BeforeMethod
	public void beforeMethod() {
		UserAccount.createUser(1);
	}

	@AfterMethod
	public void afterMethod() {
		UserAccount.delUser();
	}

	// 用例描述：上传图片规则获取
	@Test()
	public void FileRuleCreater_case1() {
		JSONObject data1 = new JSONObject();
		data1.put("createNum", "2");
		data1.put("type", "0");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_oss上传文件规则获取, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("ossEndpoint")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("secretKey")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("accessKey")));
		assertEquals(respson1.getJSONObject("data").getJSONArray("rules").size() + "", "2");
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getJSONArray("rules").getJSONObject(0).getString("bucketName")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getJSONArray("rules").getJSONObject(0).getString("accessDomain")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getJSONArray("rules").getJSONObject(0).getString("filePrefix")));
	}

	// 用例描述：上传文件规则获取
	@Test()
	public void FileRuleCreater_case2() {
		JSONObject data1 = new JSONObject();
		data1.put("createNum", "2");
		data1.put("type", "1");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_oss上传文件规则获取, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("ossEndpoint")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("secretKey")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getString("accessKey")));
		assertEquals(respson1.getJSONObject("data").getJSONArray("rules").size() + "", "2");
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getJSONArray("rules").getJSONObject(0).getString("bucketName")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getJSONArray("rules").getJSONObject(0).getString("accessDomain")));
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getJSONArray("rules").getJSONObject(0).getString("filePrefix")));
	}
}