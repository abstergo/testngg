package testcase;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import base.BaseTool;
import business.UserAccount;
import net.sf.json.JSONObject;

/**
 * 作者：王中南
 * 接口：Andriod进入充值页面查询
 * 参数：
 * */
public class RechargeForAndriod {
	String count = (String) BaseTool.querySQL("select count(id) from currency_recharge where zf=0 and status=1", "string", "count(id)");

	@BeforeMethod
	public void beforeMethod() {
		UserAccount.createUser(1);
	}

	@AfterMethod
	public void afterMethod() {
		UserAccount.delUser();
	}

	// 用例描述：正常
	@Test()
	public void RechargeForAndriod_case1() {
		JSONObject data1 = new JSONObject();
		data1.put("userId", UserAccount.userId.get(0));
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_Andriod进入充值页面查询, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getJSONObject("data").getJSONArray("rechargeForAndriod").size() + "", count);
		assertTrue(respson1.getJSONObject("data").getJSONArray("rechargeForAndriod").getJSONObject(0).getInt("amount") > 0);
		assertTrue(BaseTool.isNotEmpty(respson1.getJSONObject("data").getJSONArray("rechargeForAndriod").getJSONObject(0).getString("curname")));
		assertTrue(respson1.getJSONObject("data").getJSONArray("rechargeForAndriod").getJSONObject(0).getInt("extamout") >= 0);
		assertTrue(respson1.getJSONObject("data").getJSONArray("rechargeForAndriod").getJSONObject(0).getInt("id") > 0);
		assertTrue(respson1.getJSONObject("data").getJSONArray("rechargeForAndriod").getJSONObject(0).getDouble("price") > 0);
		assertTrue(respson1.getJSONObject("data").getJSONArray("rechargeForAndriod").getJSONObject(0).getInt("status") >= 0);
		assertTrue(respson1.getJSONObject("data").getJSONArray("rechargeForAndriod").getJSONObject(0).getInt("zf") >= 0);
		assertTrue(respson1.getJSONObject("data").getJSONObject("exchange").getInt("beExchangeRatio") > 0);
		assertTrue(respson1.getJSONObject("data").getJSONObject("exchange").getInt("exchangeRatio") > 0);
		assertTrue(respson1.getJSONObject("data").getJSONObject("exchange").getInt("exchangeRuleType") > 0);
		assertTrue(respson1.getJSONObject("data").getJSONObject("exchange").getInt("id") > 0);
		assertTrue(respson1.getJSONObject("data").getJSONObject("exchange").getInt("zf") >= 0);
		assertTrue(respson1.getJSONObject("data").getJSONObject("user").getInt("balance") >= 0);
	}
}