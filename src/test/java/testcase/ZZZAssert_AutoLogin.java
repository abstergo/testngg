package testcase;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.Test;

import base.BaseData;
import base.BaseHttpClient;
import base.BaseTool;
import business.UserAccount;
import net.sf.json.JSONObject;
import redis.clients.jedis.Jedis;

/**
 * 作者：王中南
 * 接口：自动登录
 * 参数：userType主播类型(0用户、1网红、2明星、3片场、4内部员工、5经纪人、6虚拟币用户)、sex性别(0女、1男、2未知)
 * */
public class ZZZAssert_AutoLogin {
	// 用例描述：使用正确token自动登录
	@Test()
	public void AutoLogin_case1() {
		UserAccount.createUser(1);
		//
		JSONObject data1 = new JSONObject();
		data1.put("deviceToken", "123");
		data1.put("token", BaseHttpClient.token);
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_自动登录, data1);
		assertEquals(respson1.getString("code"), "1000");
		assertEquals(respson1.getString("msg"), "成功");
	}

	// 用例描述：使用错误token自动登录
	@Test()
	public void AutoLogin_case2() {
		UserAccount.createUser(1);
		//
		JSONObject data1 = new JSONObject();
		data1.put("deviceToken", "123");
		data1.put("token", "123");
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_自动登录, data1);
		assertEquals(respson1.getString("code"), "3050");
		assertEquals(respson1.getString("msg"), "自动登录超时,请重新登录");
	}

	// 用例描述：清除redis的指定token自动登录
	@Test()
	public void AutoLogin_case3() {
		UserAccount.createUser(1);
		Jedis jedis = BaseTool.connectRedis();
		jedis.del("loginToken_" + BaseHttpClient.token);
		//
		JSONObject data1 = new JSONObject();
		data1.put("deviceToken", "123");
		data1.put("token", BaseHttpClient.token);
		JSONObject respson1 = BaseHttpClient.sendGetRequest(BaseData.URL_自动登录, data1);
		assertEquals(respson1.getString("code"), "3050");
		assertEquals(respson1.getString("msg"), "自动登录超时,请重新登录");
	}
}