package base;

import java.io.File;

public class BaseData {
	/** ----------------------------------------------------------------------------------------------------------------------------- */
	// 测试1
	public static String Environment = "";
	public static String DB_Driver = "";
	public static String DB_URL = "";
	public static String DB_User = "";
	public static String DB_Pass = "";
	public static String Redis_Host = "";
	public static int Redis_Port = 6379;
	public static String Redis_Auth = "";	

	// 日志
	public static File Log_Directory_Path = new File("D:\\盒饭接口测试日志\\");
	public static File Log_File_Path = new File("D:\\盒饭接口测试日志\\Log-" + BaseTool.getCurrentDate("MM-dd-HHmmss") + ".txt");
	public static String ExtentReport_Path = "test-output\\ExtentReport.html";

	/** ----------------------------------------------------------------------------------------------------------------------------- */
	public static String URL_手机号登录 = "http://" + Environment + "passport.hefantv.com/v2/login/userLogin";
	public static String URL_第三方登录 = "https://" + Environment + "passport.hefantv.com/v2/login/thirdLogin";

	/** ----------------------------------------------------------------------------------------------------------------------------- */
	public static String URL_Andriod进入充值页面查询 = "http://" + Environment + "pay.hefantv.com/pay/rechargeForAndriod";
	public static String URL_苹果内购支付验证 = "http://" + Environment + "";
	public static String URL_第三方登陆 = "http://" + Environment + "api.hefantv.com/v1/login/thirdLogin";
	public static String URL_编辑用户基本信息 = "http://" + Environment + "api.hefantv.com/v1/user/editUserInfo";
	public static String URL_批量关注 = "http://" + Environment + "api.hefantv.com/v1/watch/batchFork";
	public static String URL_发送短信验证码 = "http://" + Environment + "api.hefantv.com/v1/sms/send";
	public static String URL_手机号登陆 = "http://" + Environment + "api.hefantv.com/v1/login/userLogin";
	public static String URL_自动登录 = "http://" + Environment + "api.hefantv.com/v1/login/autoLogin";
	public static String URL_绑定或变更手机号与绑定第三方账号 = "http://" + Environment + "api.hefantv.com/v1/user/accountBind";
	public static String URL_解绑第三方账号 = "http://" + Environment + "api.hefantv.com/v1/user/accountUnBind";
	public static String URL_已绑定手机号进行变更手机号旧手机号短信验证 = "http://" + Environment + "api.hefantv.com/v1/user/changeSmsCodeCheck";
	public static String URL_申请主播获取开户行列表 = "http://" + Environment + "api.hefantv.com/v1/user/findSysBankList";
	public static String URL_取消关注 = "http://" + Environment + "api.hefantv.com/v1/watch/unfork";
	public static String URL_举报 = "http://" + Environment + "api.hefantv.com/v1/message/report";
	public static String URL_充值码充值 = "http://" + Environment + "pay.hefantv.com/v1/pay/rechargeCodePay";
	public static String URL_盒饭兑换 = "http://" + Environment + "pay.hefantv.com/v1/currencyExchange/doMealTicketExchange";
	public static String URL_我的页面数据 = "http://" + Environment + "api.hefantv.com/v1/user/myUserInfo";
	public static String URL_oss上传文件规则获取 = "http://" + Environment + "api.hefantv.com/pub/fileRule/fileRuleCreater";
	public static String URL_获取用户基础信息 = "http://" + Environment + "api.hefantv.com/v1/user/getUserBaseInfo";
	public static String URL_盒饭饭票兑换比例 = "http://" + Environment + "api.hefantv.com/v1/user/exchangeRule";
	public static String URL_app支付 = "http://" + Environment + "pay.hefantv.com/v1/pay/appPayOrder";
	public static String URL_ios进入充值页面查询 = "http://" + Environment + "pay.hefantv.com/pay/rechargeForIos";
	public static String URL_兑换中心 = "http://" + Environment + "api.hefantv.com/v1/user/exchangeRuleAndData";
	public static String URL_获取直播间活动信息 = "http://" + Environment + "api.hefantv.com/v1/liveActivity/getLiveActivity";
	public static String URL_轮播图跳转直播间请求接口 = "http://" + Environment + "api.hefantv.com/v1/redirect/toLiveRoom";
	public static String URL_申请主播认证提交 = "http://" + Environment + "api.hefantv.com/v2/user/userIdentitySubmit";
	public static String URL_APP设备的IDFA收集 = "http://" + Environment + "api.hefantv.com/v1/collection/idfaSubmit";
	public static String URL_验证申请主播时填写的申请码是否正确 = "http://" + Environment + "api.hefantv.com/v2/user/checkApplyCode";
	public static String URL_获取行政区列表 = "http://" + Environment + "api.hefantv.com/v2/user/getDistrictList";
	public static String URL_分享文案模版静态文件 = "http://img.hefantv.com/" + Environment + "_hefantv/ShareTextCreate.js";
	public static String URL_关注文案模版静态文件 = "http://img.hefantv.com/" + Environment + "_hefantv/FollowTextCreate.js";
	public static String URL_用户升级文案模版静态文件 = "http://img.hefantv.com/" + Environment + "_hefantv/UserUpgradeTextCreate.js";
	public static String URL_用户进场文案模版静态文件 = "http://img.hefantv.com/" + Environment + "_hefantv/EnterRoomTextCreate.js";
	public static String URL_获取轮播图列表 = "http://" + Environment + "api.hefantv.com/v1/redirect/bannerList";
	public static String URL_获取当前可恢复的直播间信息 = "http://" + Environment + "api.hefantv.com/v1/live/getLostLiveInfo";
	public static String URL_恢复直播 = "http://" + Environment + "api.hefantv.com/v1/live/recoverLiving";
	public static String URL_关闭丢失的直播 = "http://" + Environment + "api.hefantv.com/v1/live/closeLostLiving";
	public static String URL_用户关注的主播信息 = "http://" + Environment + "api.hefantv.com/v1/watch/listWatchAnchorInfo";
	public static String URL_获取直播回放 = "http://" + Environment + "api.hefantv.com/v1/message/getLivePlaybackList";
	public static String URL_登录视频底部导航按钮 = "http://" + Environment + "api.hefantv.com/v1/boot/getLoadingSrc";
	public static String URL_粉丝列表 = "http://" + Environment + "api.hefantv.com/v1/watch/listFansInfo";
	public static String URL_关注主播 = "http://" + Environment + "api.hefantv.com/v1/watch/fork";
	public static String URL_直播预告 = "http://" + Environment + "static.hefantv.com/test_hefantv/liveNotice.js";
	public static String URL_预约直播列表 = "http://" + Environment + "static.hefantv.com/tester_hefantv/liveBookingList.json";
	public static String URL_直播预约状态 = "http://" + Environment + "api.hefantv.com/v1/h5/liveBookingStatus";
	public static String URL_热门推荐 = "http://" + Environment + "api.hefantv.com/v1/friend/hotRecommend";
	public static String URL_获取系统消息列表 = "http://" + Environment + "api.hefantv.com/v1/message/getSysMsgList";
	public static String URL_获取用户关注的所有的主播的id = "http://" + Environment + "api.hefantv.com/v1/watch/listWatchId";
	public static String URL_问题反馈 = "http://" + Environment + "api.hefantv.com/v1/feedBack/submit";
	public static String URL_第一次进入俱乐部推荐明星弹窗 = "http://" + Environment + "api.hefantv.com/v1/message/recommendAnchorFirst";
	public static String URL_检查直播状态 = "http://" + Environment + "static.hefantv.com/tester_hefantv/joinLiveRoomWord.js";

	public static String URL_拉黑 = "http://" + Environment + "api.hefantv.com/v1/watch/pullBlack";
	public static String URL_取消拉黑 = "http://" + Environment + "api.hefantv.com/v1/watch/cancelBlack";
}